FROM php:7.2-fpm
USER root
# install the PHP extensions we need
RUN set -ex; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\ 
	apt-get update; \
	apt-get install -y --no-install-recommends \
	    gnupg2 \
	    && \
        /usr/bin/curl -sL https://deb.nodesource.com/setup_10.x | /bin/bash -  \
	    && \
    apt-get install -y --no-install-recommends \
	    nodejs \
		libjpeg-dev \
		libpng-dev \
		libpq-dev \
	; \
	apt-get install -y \
        mariadb-client \
	; \
    /usr/bin/curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list; \
	apt-get update; \
	apt-get install -y \
        yarn \
	; \
	\
	docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		opcache \
		pdo_mysql \
		pdo_pgsql \
		zip \
	; \
    cd /tmp && \
    curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer ; \
    cd /usr/local/etc/php && \
    ln -s php.ini-production php.ini; \
    { \
        echo 'post_max_size = 500M'; \
        echo 'upload_max_filesize = 500M'; \
        echo 'max_execution_time = 600'; \
        echo 'max_input_time = 600'; \
        echo 'default_socket_timeout = 600'; \
        echo 'opcache.memory_consumption=128'; \
        echo 'opcache.interned_strings_buffer=8'; \
        echo 'opcache.max_accelerated_files=4000'; \
        echo 'opcache.revalidate_freq=60'; \
        echo 'opcache.fast_shutdown=1'; \
        echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/zzz_custom.ini
WORKDIR /var/www
